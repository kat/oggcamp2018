# To build the presentation

`make all `

and view slides.html in a browser

# Learn about Cooperative Technologists, and the Co-operative movement

The co-operative movement offers a different way viewing the world, based on the values of self-help, self-responsibility, democracy, equality, equity and solidarity.

Co-operative organisations look to co-operate amongst themselves to effectively build and strengthen the co-operative movement, and they work for the sustainable development of their communities.

There are many similarities between the free software and free culture movements where code and content are freely given back to the community to be remixed, improved and re-engineered.

Come and hear how workers are taking control of technology industry, how you can start a workers co-operative and how you can join a tech workers co-operative.

# CoTech https://coops.tech

CoTech is a federation of 34 worker co-operatives in the tech industry sector employing more than 262 staff with total revenue greater £10m.

# CoTech Manifesto

We believe in a fairer world in which wealth and resources are distributed to the people who need them rather than those best able to take them.

As Co-operative Technologists we aim to ensure that technology plays its part in creating a fairer world.

Our individual worker co-operatives have shown that workers who collectively own their companies and control their destinies make better workplaces, better suppliers and better digital products.

We call upon tech workers who share our vision to join us.

We call upon consumers of digital products – including trades unions, charities, governments and private companies – to reject the false assumption that only multinational conglomerates or private equity-funded startups can be great tech companies. Technology is the lifeblood of all our futures, not just a gravy train for the fortunate few.

We hereby give notice to technology companies that do not treat their employees fairly, do not give their workers control of their businesses and do not seek to create a fairer world that your days are numbered. We are more creative, more committed and more resilient. Join us.
