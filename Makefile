all: 
	# pandoc -t revealjs -s -o slides.html slides.md -V revealjs-url=./reveal.js
	pandoc -t revealjs -s --self-contained -o slides.html slides.md --variable theme="og" -V revealjs-url=./reveal.js
