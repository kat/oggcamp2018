---
author: Kate Dawson
title: Cooperative Technologists @ [OggCamp 2018](https://oggcamp.org/)
date: August 18, 2018
---

# Who am I

- Kate Dawson
- Worker [Webarchitects.coop](https://webarchitects.coop)
- kate@webarchitects.coop 
- GPG: 0x01A20501E81A4BBA


<div class="notes" role="note">

I'm a worker at Webarchitects Cooperative. 

We're a small workers cooperative based, here, in Sheffield. 

We've been a cooperative for 8 years, after going through different organisational forms.

Currently we're a multistake holder cooperative 

- workers have 50% say in the running of the coop, 
- clients and partners have 25% 
- investors the remaining 25%.

</div>

# What's happening here! 

- Brief interlude about worker cooperative history
- Cooperative Principles and Free Software
- CoTech [coops.tech](https://coops.tech)
- Start your own worker coop

Content available at [git.coop/kat/oggcamp2018](https://git.coop/kat/oggcamp2018)

<div class="notes" role="note">

I'm going to talk briefly

- about the history of the cooperative movement
- the values of the cooperative movement
- How free software values can align with those of the cooperative movement
- a current contemporary phenomena, the cooperative technologists movement.
- and finally, try to persuade you to start your own worker coops.

</div>

# What's a cooperative ? 

> "an autonomous association of persons united voluntarily to meet their common economic, social, and cultural needs and aspirations through a jointly-owned and democratically-controlled enterprise"

<div class="notes" role="note">

The international cooperative association defines this very broadly as 

"an autonomous association of persons united voluntarily to meet their common economic, social, and cultural needs and aspirations through a jointly-owned and democratically-controlled enterprise"

Cooperatives are people-centred enterprises owned and run by and for their members to realise their common dreams. Profits generated are either reinvested in the enterprise or returned to the members. 

Cooperatives bring people together in a democratic and equal way. 

Whether the members are the customers, employees, users or residents, cooperatives are democratically managed by the 'one member, one vote' rule. 

Members share equal voting rights regardless of the amount of capital they put into the enterprise. 

They allow people to take control of their economic future and, because they are not owned by shareholders, the economic and social benefits of their activity stay in the communities where they are established.

</div>

# Cooperative History

- The Rochdale Society of Equitable Pioneers
    - Established 1844 as a response to conditions of the industrial revolution
    - Rochdale Principles form the basis of current cooperative values.    

<div class="notes" role="note">

The history of the modern cooperative movement starts with the Rochdale Pioneers.

The Rochdale Equitable Pioneers Society was not the first cooperative, but it's considered the first successful cooperative, and it was from this  co-operative that the modern cooperative movement was built.

The pioneers were from a broad range of 19th century working class movements which included Owenite cooperators, socialists, secularists, chartists, unitarians, teetotalers. 

Importantly, they introduced the principle of political and religious neutrality to enable the range of  working class movements to work together, in acceptance of their differences. 

They came from communities which had been devastated by the consequences of the industrial revolution, and were determined to be victims no more.

They put together a set of principles, the "Rochdale principles" which are still in use today to inform cooperative values. 


</div>

# Cooperative Principles

1. Voluntary and open membership
1. Democratic member control
1. Member economic participation
1. Autonomy and independence
1. Education, training, and information
1. Cooperation among cooperatives
1. Concern for community


<div class="notes" role="note">

The Rochdale Principles have been restated as "The cooperative principles" and are guidelines by which modern cooperatives put their values into practice.


1st Principle: Voluntary and Open Membership

Cooperatives are voluntary organisations, open to all persons able to use their services and willing to accept the responsibilities of membership, without gender, social, racial, political or religious discrimination.

2nd Principle: Democratic Member Control

Cooperatives are democratic organisations controlled by their members, who actively participate in setting their policies and making decisions. People serving as elected representatives are accountable to the membership. In primary cooperatives members have equal voting rights (one member, one vote) and cooperatives at other levels are also organised in a democratic manner.

3rd Principle: Member Economic Participation

Members contribute equitably to, and democratically control, the capital of their cooperative. At least part of that capital is usually the common property of the co- operative. Members usually receive limited compensation, if any, on capital subscribed as a condition of membership. Members allocate surpluses for any or all of the following purposes: developing their cooperative, possibly by setting up reserves, part of which at least would be indivisible; benefiting members in proportion to their transactions with the cooperative; and supporting other activities approved by the membership.

4th Principle: Autonomy and Independence

Cooperatives are autonomous, self-help organisations controlled by their members. If they enter into agreements with other organisations, including governments, or raise capital from external sources, they do so on terms that ensure democratic control by their members and maintain their cooperative autonomy.

5th Principle: Education, Training and Information

Cooperatives provide education and training for their members, elected representatives, managers, and employees so they can contribute effectively to the development of their cooperatives. They inform the general public - particularly young people and opinion leaders - about the nature and benefits of cooperation.

6th Principle: Cooperation among Cooperatives

Cooperatives serve their members most effectively and strengthen the cooperative movement by working together through local, national, regional and international structures.

7th Principle: Concern for Community

Cooperatives work for the sustainable development of their communities through policies approved by their members.

</div>

# Examples of Coops

Cooperatives in the UK can take various forms, including 

- companies limited by guarantee
- companies limited by share
- cooperative and mutual societies
- partnerships and unicorporated bodies

<div class="notes" role="note">


Cooperatives in the UK can take various forms, including

- companies limited by guarantee
- companies limited by share
- cooperative and mutual societies
- partnerships and unicorporated bodies


So we see that coops come in all sorts of shapes and sizes, but a common thread is that

Cooperatives are based on the values of self-help, self-responsibility, democracy, equality, equity and solidarity. In the tradition of their founders, co- operative members believe in the ethical values of honesty, openness, social responsibility and caring for others.

</div>


# Examples of Coops

- [Swann-Morton](https://www.swann-morton.com/company.php)
    - Large engineering firm manafactuing surgical blades
    - Established 1932
    - "employees had a 50% share and the remaining 50% placed in a charitable trust"
    - "Claims of individuals producing in an industry came first, before anything else, and must always remain first. They are the human beings on which everything is built."

<div class="notes" role="note">

For example Swann-Morton is a large Sheffield engineering company established in 1932,  manafacturing surgical blades. 

It is "adminstrated by a trust ... with employees having a 50% share and the remaining 50% placed in a charitable trust" 

The company has the founding principle statment 

"Claims of individuals producing in an industry came first, before anything else, and must always remain first. They are the human beings on which everything is built."


</div>

# Examples of Coops

- [Suma.coop](https://suma.coop)
    - Largest indepedent wholefood wholesaler in the UK
    - Equal pay employer

<div class="notes" role="note">

Suma coop is the largest independent wholefood wholesaler in the UK, with over 160 workers.

Everyone at Suma gets paid the same wage, regardless of what job they do. The largest equal pay employer in Europe.

Additionally, it practices job rotation, where everyone performs more than one role in the coop.

So we see that cooperatives can have quite radical and different structures, whilst still meeting the definition.

</div>

# [Free Software](http://fsfe.org/about/basics/freesoftware.en.html)


0. Freedom to use it
0. Freedom to study it
0. Freedom to share it
0. Freedom to adapt it


<div class="notes" role="note">

I'm sure everyone here is familiar with the Free Software Four Freedoms.

0. Freedom to use it
0. Freedom to study it
0. Freedom to share it
0. Freedom to adapt it

But we can think about Free Software, and the freedoms it gives us, and how those freedoms can be used by cooperatives to help provide their needs for autonomy and democracy, and the meeting of their economic/social/cultural needs.

Well clearly these require the freedoms to use, study, share and to adapt. 

If we do not have them how can we be autonomous, or exercise our democratic rights as members within the our coops. 

</div>

# Coop Principles and Free Software

- Principle 3: Member Economic Participation
    - "Surpluses ... part of which at least would be indivisible"

- 6th Principle: Cooperation among Cooperatives

- Principle 7: Concern for Community
    - "Cooperatives work for sustainable development of their communities"


<div class="notes" role="note">

Perhaps more interestingly is the ways that the cooperative principles can be applied to Free software and technology production. 

Principle 3 mentions investing our surpluses, in ways that are indivisable. 

Being indivisible means that the surplus is part of a commonwealth for all. It can't be privatised or locked off, even in the event that the cooperative ceases to exist.
The indivisbility is enshrined in Law, in the articles of association of the Coop.

To me this looks very much like the GPL hack, which allows software to be modified and improved, but prevents it from being appropriated behind restrictive licenses.

Principle 6: Cooperation amongst cooperatives. 

When a coop finds a need or service which it requires that it can't fulfill itself, it will try to find another cooperative that can provide it. 

When one can't find a solution within a piece of techology, one can look to see if another Free software project can be used, before looking at proprietary or other solutions. 

Principle 7: This looks like being a responsible member of Free software community, feeding back bug reports and fixes, donating to projects, and providing documentation. 

For example at Webarchitects

- We publish our Ansible scripts
- We donate to Free software projects we use for infrastructure
- We publish bug reports and solutions we find. 

</div>


# Tech coops

## Why do we need them ?

What has the tech industry given us ? 

- [Closed Hardware](https://www.fsf.org/blogs/community/the-apple-is-still-rotten-why-you-should-avoid-the-new-iphone)
- [Surveillance Capitalism](https://www.opendemocracy.net/uk/jennifer-cobbe/problem-isn-t-just-cambridge-analytica-or-even-facebook-it-s-surveillance-capitali)
- [Tax dodging](https://www.theguardian.com/commentisfree/2017/dec/11/tech-giants-taxes-apple-paradise-corporation-avoidance)  

<div class="notes" role="note">

What has tech industry given us?

- Closed hardware and software that users are not able to modify
    - For example Apple, has been attempting to stop legislation allowing people access to information so they can repair equipment.
    - Apple and other tech industry bodies have stopped the Nebraska "Fair Repair" bill in 2017 which would have allowed people access to manuals and diagnostic tools to repair electronic equipment
- The greatest and most intrusive surveillance society of all time.
    - Facebook and Cambridge Analytica have millions of data points on millions of people
    - Google tracks your location, even when you opt out
- The largest most powerful corporations of all time
   - Amazon pays £1.7m in UK Tax on a turnover of £1.98 billion ( https://www.bbc.co.uk/news/business-45053528)
   - Increase in the concentration of wealth and inequity

These are not the actions of corporations that care for communities.  

If we are looking to build a world that empowers and builds sustainable supportive communities the cooperative model, with its ethical principles built in is attractive.

</div>


# Fixing the tech industry with coops

- Caring is inherent
- Worker control
- Technology users can be part of the cooperatives


<div class="notes" role="note">

- Care for our communities is built in to cooperatives via their principles
- Workers are able to make decisions without being controlled by bosses or shareholders
- The workers are part of the community, hence unlikely to act against it's interest.
- If Facebook ( for example) was controlled by it's users would they have voted for surveillance ? 
- Instead of extracting money our communities to distant shareholders, cooperatives seek to invest their surplus for the community benefit. 


</div>

<!--
# Tech coops

## Better for workers ? 

- Provide empowerment (You can make decisions with others)
- Support and Solidarity (You are not alone as a freelancer)

## Better for customers
- Worker coops are more productive

<div class="notes" role="note">

[Research by Virginie Pérotin, Professor of Economics at  Leeds University Business School](https://www.uk.coop/sites/default/files/uploads/attachments/worker_co-op_report.pdf), has produced reports showing that 

- worker coops are more productive, 
- have smaller pay differentials between executive and non-executive workers.


</div>
-->

# Platform coops

Cooperatives that connect consumers and producers.

- Multistake holder cooperatives with different categories of members
- Example
    - Fairmondo.uk
        - A platform for ethically produced products 
    - social.coop
        - A "twitter" like social network based on Mastodon
    - coopcycle.org
        - an open source food delivery app used by collectives of cycle couriers
      


<div class="notes" role="note">

Platform cooperative are a response to the platform capitalism of companies like Uber, Deliveroo

Cooperatives that connect consumers and producers.

- Multistake holder cooperatives with different categories of members
- Example
    - Fairmondo.uk
        - A platform for ethically produced products 
    - social.coop
        - A "twitter" like social network based on Mastodon,  decentralised social network
    - coopcycle.org
        - an open source food delivery app used by collectives of cycle couriers
      
</div>



# [Coops.tech](https://coops.tech)


## What is it  ?

<!-- <div style="float: left"> <img src=images/CT_Logo_FA-02.svg style="width: 10em;"> </div> -->


> "Cooperative Technologists (aka CoTech) is a network of cooperatives that sell tech/digital services. It aims to create a better technology sector in the UK that focuses primarily on worker, customer and end-user needs rather than on generating private profit."

<div class="notes" role="note">

"Cooperative Technologists (aka CoTech) is a network of cooperatives that sell tech/digital services. It aims to create a better technology sector in the UK that focuses primarily on worker, customer and end-user needs rather than on generating private profit."

Currently 35 different cooperatives, all with different cooperative organisational structures, but all owned and controlled by their workers and in some cases their customers. 

All working with different technologies, but attempting to work together to increase the market share of companies that are owned by their workers.

</div>

# [Coops.tech](https://coops.tech)

## History 

During 2016 Harry Robbins of [outlandish.coop](https://outlandish.coop) started doing some outreach amongst the tech coop community with ambitious plans to build a collaborative organisation, the "Megazord". 


Out of this came a 2 day conference in Wortley Hall, and foundation of the network.

<div class="notes" role="note">

During 2016 Harry Robbins of [outlandish.coop](https://outlandish.coop) started doing some outreach amongst the tech coop community with ambitious plans to build a collaborative organisation, the "Megazord".


Out of this came a 2 day conference in Wortley Hall, and foundation of the network.


Megazord: A codename for "a UK-based coalition of tech-focused cooperatives that pool their resources to achieve their shared aims – making the world better and fairer with technology."

Wortley Hall is a cooperatively owned stately home, just south of Barnsley

Since then there has been another event at Wortley Hall, a 4 day conference, in 2017,  which has helped grow and expand the network.

</div>

# What's in the Network ?

## Virtual Resources

- Loomio (Decision making)
- Wiki [wiki.coops.tech](https://wiki.coops.tech)
- [git.coop](https://git.coop)
- Discourse Forum [community.coops.tech](https://community.coops.tech)


<div class="notes" role="note">

Whats in the network ?

There are a number of virtual resources.

- Loomio (Decision making)
- Wiki [wiki.coops.tech](https://wiki.coops.tech)
- [git.coop](https://git.coop)
- Discourse Forum [community.coops.tech](https://community.coops.tech)

Additionally there is a cobudgeting application that is in development, where financial surpluses can be invested in new projects.


</div>

# Physical resources

## [space4.tech](https://space4.tech)

- Coworking/Event space
- Finsbury Park, London

<div class="notes" role="note">

There are physical resources too. 

Space4.tech is a coworking space, hosting organisations and individuals who are engaged in using technology to build a fairer society. 
Particularly young cooperatives who are dedicated to creating social value.

Used by 3 CoTech cooperatives.

</div>

# Joining Cotech

- Existing Tech Coops
- Existing Tech/Digital Company
- Individuals that want to work in coops 
- Individuals that don't want to work for coops

<div class="notes" role="note">

If your are already part of a tech coop, or an organisation that is practicing cooperative principles and is in the process of becoming a coop, then get in touch!

If you are part of a company or social enterprise that sells digital services and think that working cooperatively is great then get in touch! There are lots of people in the network with skills and experience in starting coops

If you are an individual that wants to work for an existing cooperative then get in touch! There may be jobs in the network.  

Even if you are not interested in working for a coop, get in touch! You could still support our efforts for example by raising awareness of the cooperative model, and helping shape the network.

</div>
<!-- 
# Start a Cooperative

<img src="images/Co-operative_drivers.png" style="max-width: 75%; max-height: 75%;">

<div class="notes" role="note">

Produced at Wortley Hall 2017

- Active Drivers:
- Reactive Drivers


</div>

# Start a Cooperative

Find a cooperative development group ? 

### Eg.
 
- [Somerset Cooperative Services](https://somerset.coop)
- [Sheffield coop development group](https://www.scdg.org)

# Differences from Startup

## Startups

- Grow fast
- VC Funding
- Get bought out


<div class="notes" role="note">

Whilst working within these organisations can be quite non-heirarchical on the surface, they lack the democratic control; power comes down to the investors and founders.

The also lack the sustainable community development goals that cooperatives have.

</div>

-->

<!--
# Differences from Social Enteprises

# Start a Coop

## How bullshit is your job
-->


# Cooperate

> We call upon consumers of digital products – including trades unions, charities, governments and private companies – to reject the false assumption that only multinational conglomerates or private equity-funded startups can be great tech companies. Technology is the lifeblood of all our futures, not just a gravy train for the fortunate few.



# Thanks 

- [outlandish.coop](https://outlandish.coop)
- [sheffield.coop](https://sheffield.coop)
- [software.coop](https://software.coop)
- [ica.coop](https://ica.coop)
- [coops.tech](https://coops.tech)
